#Roundcube plugin for piwik_analytics

Easy setup for piwik Analytics in Roundcube Webmail. This is fork of [roundcube_google_analytics](https://github.com/igloonet/roundcube_google_analytics/)

## Installation

Just install in plugins/piwik_analytics and add piwik_analytics to your `$rcmail_config['plugins']` array

## Features

* Set your piwik site ID
* Enable only for non-logged users (optional)
* Disable on choosen templates

## License

This plugin is released under GNU GPL
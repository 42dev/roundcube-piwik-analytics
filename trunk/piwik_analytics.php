<?php

/**
 * 
 * piwik_analytics
 *
 * Bind piwik analytics script - based on: http://github.com/igloonet/roundcube_google_analytics
 *
 * @version 1.0 - 28. 11. 2010
 * @author Florian Beer
 * @modified_by Florian Beer
 * @website http://blog.no-panic.at
 * @licence GNU GPL
 *
 *
 **/

class piwik_analytics extends rcube_plugin
{
  function init()
  {
    if(file_exists("./plugins/piwik_analytics/config/config.inc.php"))
      $this->load_config('config/config.inc.php');
    else
      $this->load_config('config/config.inc.php.dist');
    $this->add_hook('render_page', array($this, 'add_script'));
  }

  function add_script($args){
    $rcmail = rcmail::get_instance();
    $exclude = array_flip($rcmail->config->get('piwik_analytics_exclude'));
    
    if(isset($exclude[$args['template']]))
      return $args;
    if($rcmail->config->get('piwik_analytics_privacy')){
      if(!empty($_SESSION['user_id']));
        return $args;
    }
    
    if(!$rcmail->config->get('piwik_analytics_domain')) return $args;
    
      $script = '
<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://' . $rcmail->config->get('piwik_analytics_domain') . '/" : "http://' . $rcmail->config->get('piwik_analytics_domain') . '/");
document.write(unescape("%3Cscript src=\'" + pkBaseURL + "piwik.js\' type=\'text/javascript\'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", ' . $rcmail->config->get('piwik_analytics_id') . ');
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://' . $rcmail->config->get('piwik_analytics_domain') . '/piwik.php?idsite=' . $rcmail->config->get('piwik_analytics_id') . '" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tag -->
    ';
    
      // add script to end of page
      $rcmail->output->add_footer($script);
     
    return $args;
  }
}

?>

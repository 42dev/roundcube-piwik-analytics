<?php

/* piwik site id */
$rcmail_config['piwik_analytics_id'] = 5;

/* domain of your piwik installation */
$rcmail_config['piwik_analytics_domain'] = 'stats.no-panic.at';

/* exclude private pages - analytics will works only for non-logged users */
$rcmail_config['google_analytics_privacy'] = FALSE;

/* exclude the following templates */
$rcmail_config['piwik_analytics_exclude'] = array(
  "message",
  "messagepreview",
  "compose",
  "editidentity",
  "editcontact", 
  "showcontact"
);

//  Possible templates:

//  "message",
//  "messagepart",
//  "messagepreview",
//  "messageerror",
//  "showcontact",
//  "addcontact",
//  "editcontact",
//  "importcontacts",
//  "addressbook",
//  "compose",
//  "editidentity",
//  "error",
//  "identities",
//  "login",
//  "mail",
//  "managefolders",
//  "plugin",
//  "settingsedit",
//  "settings"
